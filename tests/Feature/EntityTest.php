<?php

namespace Tests\Feature;

use App\Http\Controllers\EntityController;
use App\Models\Entity;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EntityTest extends TestCase
{
    use RefreshDatabase;

    public function testCreateEntity()
    {
        $data = ['name' => 'Test', 'description' => 'Description.'];

        $response = $this->post(action([EntityController::class, 'create']), $data);

        $response->assertOk();
        $response->assertJsonFragment([
            'id' => Entity::first()->id
        ]);
        $this->assertDatabaseCount('entities', 1);
        $this->assertDatabaseHas('entities', $data);
    }

    public function testGetEntities()
    {
        $entities = Entity::factory(2)->create();

        $response = $this->get(action([EntityController::class, 'index']));

        $response->assertOk();
        $response->assertJsonFragment([
            'id' => $entities[0]->id,
            'name' => $entities[0]->name,
            'description' => $entities[0]->description
        ]);
        $response->assertJsonFragment([
            'id' => $entities[1]->id,
            'name' => $entities[1]->name,
            'description' => $entities[1]->description
        ]);
    }
}
