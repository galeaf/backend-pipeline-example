<?php

namespace Tests\Unit;

use App\Domain\Repository;
use Tests\TestCase;

class RepositoryTest extends TestCase
{
    public function testGetItems()
    {
        $items = ['foo' => 'bar', 'baz' => 'biz'];

        $repository = new Repository($items);

        $this->assertEquals($items, $repository->items());
    }

    public function testGetItemByName()
    {
        $items = ['foo' => 'bar', 'baz' => 'biz'];

        $repository = new Repository($items);

        $this->assertEquals($items['foo'], $repository->getByName('foo'));
    }
}
