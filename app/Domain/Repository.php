<?php

namespace App\Domain;

class Repository
{
    private array $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public function items(): array
    {
        return $this->items;
    }

    public function getByName($name)
    {
        return $this->items[$name] ?? null;
    }
}
