<?php

namespace App\Http\Controllers;

use App\Models\Entity;
use Illuminate\Http\Request;

class EntityController extends Controller
{
    public function create(Request $request)
    {
        $entity = Entity::create($request->only(['name', 'description']));

        return ['id' => $entity->id];
    }

    public function index()
    {
        return Entity::all()->toArray();
    }
}
